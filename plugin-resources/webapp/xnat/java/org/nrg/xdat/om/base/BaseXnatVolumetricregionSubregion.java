// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Wed Jun 28 12:57:55 CDT 2006
 *
 */
package org.nrg.xdat.om.base;
import java.util.Hashtable;

import org.nrg.xdat.om.base.auto.AutoXnatVolumetricregionSubregion;
import org.nrg.xft.ItemI;
import org.nrg.xft.security.UserI;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class BaseXnatVolumetricregionSubregion extends AutoXnatVolumetricregionSubregion {

	public BaseXnatVolumetricregionSubregion(ItemI item)
	{
		super(item);
	}

	public BaseXnatVolumetricregionSubregion(UserI user)
	{
		super(user);
	}

	public BaseXnatVolumetricregionSubregion()
	{}

	public BaseXnatVolumetricregionSubregion(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
