/*
 * GENERATED FILE
 * Created on Mon Nov 22 10:20:49 CST 2010
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseValProtocoldataScanCheckComment extends AutoValProtocoldataScanCheckComment {

	public BaseValProtocoldataScanCheckComment(ItemI item)
	{
		super(item);
	}

	public BaseValProtocoldataScanCheckComment(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseValProtocoldataScanCheckComment(UserI user)
	 **/
	public BaseValProtocoldataScanCheckComment()
	{}

	public BaseValProtocoldataScanCheckComment(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
