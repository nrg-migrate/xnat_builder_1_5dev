/*
 * GENERATED FILE
 * Created on Mon Nov 22 10:20:49 CST 2010
 *
 */
package org.nrg.xdat.om.base;
import org.nrg.xdat.om.base.auto.*;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public abstract class BaseValProtocoldataComment extends AutoValProtocoldataComment {

	public BaseValProtocoldataComment(ItemI item)
	{
		super(item);
	}

	public BaseValProtocoldataComment(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseValProtocoldataComment(UserI user)
	 **/
	public BaseValProtocoldataComment()
	{}

	public BaseValProtocoldataComment(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
