// Copyright 2010 Washington University School of Medicine All Rights Reserved
package org.nrg.xnat.restlet.resources;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

import org.nrg.xdat.base.BaseElement;
import org.nrg.xdat.model.XnatExperimentdataShareI;
import org.nrg.xdat.model.XnatProjectdataI;
import org.nrg.xdat.om.XnatExperimentdata;
import org.nrg.xdat.om.XnatExperimentdataShare;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.om.XnatSubjectassessordata;
import org.nrg.xdat.om.XnatSubjectdata;
import org.nrg.xft.XFTItem;
import org.nrg.xft.XFTTable;
import org.nrg.xft.db.DBAction;
import org.nrg.xft.db.MaterializedView;
import org.nrg.xft.exception.InvalidValueException;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.SaveItemHelper;
import org.nrg.xft.utils.StringUtils;
import org.nrg.xft.utils.ValidationUtils.ValidationResults;
import org.nrg.xnat.archive.Rename;
import org.nrg.xnat.archive.Rename.DuplicateLabelException;
import org.nrg.xnat.archive.Rename.FolderConflictException;
import org.nrg.xnat.archive.Rename.LabelConflictException;
import org.nrg.xnat.archive.Rename.ProcessingInProgress;
import org.nrg.xnat.archive.ValidationException;
import org.nrg.xnat.exceptions.InvalidArchiveStructure;
import org.nrg.xnat.helpers.xmlpath.XMLPathShortcuts;
import org.nrg.xnat.restlet.actions.FixScanTypes;
import org.nrg.xnat.restlet.actions.PullSessionDataFromHeaders;
import org.nrg.xnat.restlet.actions.TriggerPipelines;
import org.nrg.xnat.restlet.util.XNATRestConstants;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Representation;
import org.restlet.resource.StringRepresentation;
import org.restlet.resource.Variant;
import org.xml.sax.SAXException;

public class SubjAssessmentResource extends SubjAssessmentAbst {
	XnatProjectdata proj=null;
	XnatSubjectdata subject=null;
	XnatSubjectassessordata expt = null;
	String exptID=null;
	XnatSubjectassessordata existing;
	String subID= null;
	
	public SubjAssessmentResource(Context context, Request request, Response response) {
		super(context, request, response);
		
			String pID= (String)getParameter(request,"PROJECT_ID");
			if(pID!=null){
				proj = XnatProjectdata.getProjectByIDorAlias(pID, user, false);
		}
		
		if(proj==null){
			response.setStatus(Status.CLIENT_ERROR_NOT_FOUND);
			return;
			}

			subID= (String)getParameter(request,"SUBJECT_ID");
			if(subID!=null){
			subject = XnatSubjectdata.GetSubjectByProjectIdentifier(proj
					.getId(), subID, user, false);
				
				if(subject==null){
				subject = XnatSubjectdata.getXnatSubjectdatasById(subID, user,
						false);
				if (subject != null
						&& (proj != null && !subject.hasProject(proj.getId()))) {
					subject = null;
				}
				}
			}
			
			exptID= (String)getParameter(request,"EXPT_ID");
			if(exptID!=null){
			if (proj != null) {
				if (existing == null) {
					existing = (XnatSubjectassessordata) XnatExperimentdata
							.GetExptByProjectIdentifier(proj.getId(), exptID,
									user, false);
			}
			}

			if (existing == null) {
				existing = (XnatSubjectassessordata) XnatExperimentdata
						.getXnatExperimentdatasById(exptID, user, false);
				if (existing != null
						&& (proj != null && !existing.hasProject(proj.getId()))) {
					existing = null;
				}
			}

			this.getVariants().add(new Variant(MediaType.TEXT_HTML));
			this.getVariants().add(new Variant(MediaType.TEXT_XML));
		}else{
			response.setStatus(Status.CLIENT_ERROR_NOT_FOUND);
		}
		this.fieldMapping.putAll(XMLPathShortcuts.getInstance().getShortcuts(XMLPathShortcuts.EXPERIMENT_DATA,false));
	}



	@Override
	public boolean allowPut() {
		return true;
	}

	@Override
	public void handlePut() {
	        XFTItem item = null;			

			try {
			XFTItem template=null;
			if (existing!=null){
				template=existing.getItem();
			}

			item=this.loadItem(null,true,template);
			
				if(item==null){
					String xsiType=this.getQueryVariable("xsiType");
					if(xsiType!=null){
						item=XFTItem.NewItem(xsiType, user);
					}
				}
				
				if(item==null){
				if(proj!=null){
					XnatSubjectassessordata om =(XnatSubjectassessordata)XnatSubjectassessordata.GetExptByProjectIdentifier(proj.getId(), this.exptID,user, false);
					if(om!=null){
						item=om.getItem();
					}
				}

				if(item==null){
					XnatSubjectassessordata om =(XnatSubjectassessordata)XnatSubjectassessordata.getXnatExperimentdatasById(this.exptID, null, false);
					if(om!=null){
						item=om.getItem();
					}
				}
			}

			if(item==null){
				this.getResponse().setStatus(Status.CLIENT_ERROR_EXPECTATION_FAILED, "Need PUT Contents");
					return;
				}
				
				if(item.instanceOf("xnat:subjectAssessorData")){
					expt = (XnatSubjectassessordata)BaseElement.GetGeneratedItem(item);
					
				if(filepath!=null && !filepath.equals("")){
					if(filepath.startsWith("projects/")){
						String newProjectS= filepath.substring(9);
						XnatProjectdata newProject=XnatProjectdata.getXnatProjectdatasById(newProjectS, user, false);
						String newLabel = this.getQueryVariable("label");
						if(newProject!=null){
							if(expt.getProject().equals(newProject.getId())){
								this.getResponse().setStatus(Status.CLIENT_ERROR_CONFLICT,"Already assigned to project:"+ newProject.getId());
								return;
							}
							
							if(!user.canRead(expt)){
								this.getResponse().setStatus(Status.CLIENT_ERROR_NOT_FOUND,"Specified user account has insufficient priviledges for experiments in this project.");
								return;
							}

							int index=0;
							XnatExperimentdataShare matched=null;
							for(XnatExperimentdataShareI pp : expt.getSharing_share()){
								if(pp.getProject().equals(newProject.getId())){
									matched=(XnatExperimentdataShare)pp;
									if(newLabel!=null && !pp.getLabel().equals(newLabel)){
										((XnatExperimentdataShare)pp).setLabel(newLabel);
										SaveItemHelper.authorizedSave(((XnatExperimentdataShare)pp),user,false,false);
									}
									break;
								}
								index++;
							}

							if(this.getQueryVariable("primary")!=null && this.getQueryVariable("primary").equals("true")){
								if(newLabel==null || newLabel.equals(""))newLabel=expt.getLabel();
								if(newLabel==null || newLabel.equals(""))newLabel=expt.getId();

								
								if(!user.canDelete(expt)){
									this.getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN,"Specified user account has insufficient priviledges for experiments in this project.");
									return;
								}
								
								XnatExperimentdata match=XnatExperimentdata.GetExptByProjectIdentifier(newProject.getId(), newLabel,user, false);
								if(match!=null){
									this.getResponse().setStatus(Status.CLIENT_ERROR_CONFLICT,"Specified label is already in use.");
									return;
								}

								expt.moveToProject(newProject,newLabel,user);

								if(matched!=null){
									SaveItemHelper.authorizedRemoveChild(expt.getItem(), "xnat:experimentData/sharing/share", matched.getItem(), user);
									expt.removeSharing_share(index);
								}
							}else{
								if(matched==null){

									if(newLabel!=null){
										XnatExperimentdata temp=XnatExperimentdata.GetExptByProjectIdentifier(newProject.getId(), newLabel, null, false);
										if(temp!=null){
											this.getResponse().setStatus(Status.CLIENT_ERROR_CONFLICT,"Label already in use:"+ newLabel);
											return;
										}
									}
										if(user.canCreate(expt.getXSIType()+"/project", newProject.getId())){
											XnatExperimentdataShare pp= new XnatExperimentdataShare((UserI)user);
											pp.setProject(newProject.getId());
											if(newLabel!=null)pp.setLabel(newLabel);
											pp.setProperty("sharing_share_xnat_experimentda_id", expt.getId());
											SaveItemHelper.authorizedSave(pp,user, false, false);
										}else{
											this.getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN,"Specified user account has insufficient create priviledges for experiments in the " + newProject.getId() + " project.");
											return;
										}
								}else{
									this.getResponse().setStatus(Status.CLIENT_ERROR_CONFLICT,"Already assigned to project:"+ newProject.getId());
									return;
								}
							}
							

							this.returnDefaultRepresentation();
						}else{
							this.getResponse().setStatus(Status.CLIENT_ERROR_NOT_FOUND,"Unable to identify project: " + newProjectS);
							return;
						}
					}else{
							this.getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
							return;
						}
				}else{
					if(expt.getLabel()==null){
						expt.setLabel(this.exptID);
					}
					//MATCH PROJECT
					if(this.proj==null && expt.getProject()!=null){
						proj = XnatProjectdata.getXnatProjectdatasById(expt.getProject(), user, false);
					}
					
					if(this.proj!=null){
						if(expt.getProject()==null || expt.getProject().equals("")){
							expt.setProject(this.proj.getId());
						}else if(expt.getProject().equals(this.proj.getId())){
						}else{
							boolean matched=false;
							for(XnatExperimentdataShareI pp : expt.getSharing_share()){
								if(pp.getProject().equals(this.proj.getId())){
									matched=true;
									break;
								}
							}
							
							if(!matched){
								XnatExperimentdataShare pp= new XnatExperimentdataShare((UserI)user);
								pp.setProject(this.proj.getId());
								expt.setSharing_share(pp);
							}
						}
					}else{
						this.getResponse().setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY,"Submitted experiment record must include the project attribute.");
						return;
					}
					
					//FIND PRE-EXISTING
					if(existing==null){
						if(expt.getId()!=null){
							existing=(XnatSubjectassessordata)XnatExperimentdata.getXnatExperimentdatasById(expt.getId(), null, completeDocument);
						}

						if(existing==null && expt.getProject()!=null && expt.getLabel()!=null){
							existing=(XnatSubjectassessordata)XnatExperimentdata.GetExptByProjectIdentifier(expt.getProject(), expt.getLabel(),user, completeDocument);
						}

						if(existing==null){
							for(XnatExperimentdataShareI pp : expt.getSharing_share()){
								existing=(XnatSubjectassessordata)XnatExperimentdata.GetExptByProjectIdentifier(pp.getProject(), pp.getLabel(),user, completeDocument);
								if(existing!=null){
									break;
								}
							}
						}
					}

					//MATCH SUBJECT
					if(this.subject!=null){
							expt.setSubjectId(this.subject.getId());
					}else{
						if(StringUtils.IsEmpty(expt.getSubjectId()) && org.apache.commons.lang.StringUtils.isNotEmpty(subID)){
							expt.setSubjectId(subID);
						}

						if(expt.getSubjectId()!=null && !expt.getSubjectId().equals("")){
							this.subject=XnatSubjectdata.getXnatSubjectdatasById(expt.getSubjectId(), user, false);
							
							if(this.subject==null && expt.getProject()!=null && expt.getLabel()!=null){
								this.subject=XnatSubjectdata.GetSubjectByProjectIdentifier(expt.getProject(), expt.getSubjectId(),user, false);
							}
							
							if(this.subject==null){
								for(XnatExperimentdataShareI pp : expt.getSharing_share()){
									this.subject=XnatSubjectdata.GetSubjectByProjectIdentifier(pp.getProject(), expt.getSubjectId(),user, false);
									if(this.subject!=null){
										break;
									}
								}
							}

							if(subject==null && existing!=null){
								this.subject=existing.getSubjectData();
								expt.setSubjectId(subject.getId());
					}
					
							if(this.subject==null){
								this.subject = new XnatSubjectdata((UserI)user);
								this.subject.setProject(this.proj.getId());
								this.subject.setLabel(expt.getSubjectId());
								this.subject.setId(XnatSubjectdata.CreateNewID());
								if(!user.canCreate(this.subject)){
									this.getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN,"Specified user account has insufficient create priveledges for subjects in this project.");
									return;
					}
								SaveItemHelper.authorizedSave(this.subject,user, false, true);
								expt.setSubjectId(this.subject.getId());
							}
						}
					}
					
					if(existing==null){
						if(!user.canCreate(expt)){
							this.getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN,"Specified user account has insufficient create priviledges for experiments in this project.");
							return;
						}
						//IS NEW
						if(expt.getId()==null || expt.getId().equals("")){
							expt.setId(XnatExperimentdata.CreateNewID());
						}
					}else{
						if(expt.getId()==null || expt.getId().equals("")){
							expt.setId(existing.getId());
						}

						//MATCHED
						if(!existing.getProject().equals(expt.getProject())){
							this.getResponse().setStatus(Status.CLIENT_ERROR_CONFLICT,"Project must be modified through seperate URI.");
							return;
						}

						if(!user.canEdit(expt)){
							this.getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN,"Specified user account has insufficient edit priviledges for experiments in this project.");
							return;
						}

						if(this.getQueryVariable("label")!=null && !this.getQueryVariable("label").equals("") ){
							if(!expt.getLabel().equals(existing.getLabel())){
								expt.setLabel(existing.getLabel());
						}

							String label=this.getQueryVariable("label");

							if(!label.equals(existing.getLabel())){
								XnatExperimentdata match=XnatExperimentdata.GetExptByProjectIdentifier(proj.getId(), label,user, false);
								if(match!=null){
									this.getResponse().setStatus(Status.CLIENT_ERROR_CONFLICT,"Specified label is already in use.");
									return;
								}

								Rename renamer = new Rename(proj,existing,label,user);
								try {
									renamer.call();
								} catch (ProcessingInProgress e) {
									logger.error("", e);
									this.getResponse().setStatus(Status.CLIENT_ERROR_CONFLICT,"Specified session is being processed (" + e.getPipeline_name() +").");
									return;
								} catch (DuplicateLabelException e) {
									logger.error("", e);
									this.getResponse().setStatus(Status.CLIENT_ERROR_CONFLICT,"Specified label is already in use.");
									return;
								} catch (LabelConflictException e) {
									logger.error("", e);
									this.getResponse().setStatus(Status.CLIENT_ERROR_CONFLICT,"Specified label is already in use.");
									return;
								} catch (FolderConflictException e) {
									logger.error("", e);
									this.getResponse().setStatus(Status.CLIENT_ERROR_CONFLICT,"File system destination contains pre-existing files");
									return;
								} catch (InvalidArchiveStructure e) {
									logger.error("", e);
									this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"Non-standard archive structure in existing experiment directory.");
									return;
								} catch (URISyntaxException e) {
									logger.error("", e);
									this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,"Non-standard archive structure in existing experiment directory.");
									return;
								} catch (Exception e) {
									logger.error("", e);
									this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,e.getMessage());
									return;
								}
							}
							return;
						}
					}

					if(this.subject==null){
						this.getResponse().setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY,"Submitted experiment record must include the subject.");
						return;
					}
					
					boolean allowDataDeletion=false;
					if(this.getQueryVariable("allowDataDeletion")!=null && this.getQueryVariable("allowDataDeletion").equals("true")){
						allowDataDeletion=true;
					}
					
					if(this.isQueryVariableTrue(XNATRestConstants.FIX_SCAN_TYPES) || this.containsAction(XNATRestConstants.FIX_SCAN_TYPES)){
						if(expt instanceof XnatImagesessiondata){
							FixScanTypes fst=new FixScanTypes(expt,user,proj,false);
							fst.call();
						}
					}


					if(!StringUtils.IsEmpty(expt.getLabel()) && !StringUtils.IsAlphaNumericUnderscore(expt.getId())){
						this.getResponse().setStatus(Status.CLIENT_ERROR_EXPECTATION_FAILED,"Invalid character in experiment label.");
						return;
					}
					
					final ValidationResults vr = expt.validate();
		            
		            if (vr != null && !vr.isValid())
		            {
		            	this.getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,vr.toFullString());
						return;
		            }
					
					if(SaveItemHelper.authorizedSave(expt,user,false,allowDataDeletion)){
						user.clearLocalCache();
					MaterializedView.DeleteByUser(user);

						if(this.proj.getArcSpecification().getQuarantineCode()!=null && this.proj.getArcSpecification().getQuarantineCode().equals(1)){
							expt.quarantine(user);
						}
					}

					if(this.getQueryVariable("activate")!=null && this.getQueryVariable("activate").equals("true")){
						if(user.canActivate(expt.getItem()))expt.activate(user);
						else this.getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN,"Specified user account has insufficient activation priviledges for experiments in this project.");
					}

					if(this.getQueryVariable("quarantine")!=null && this.getQueryVariable("quarantine").equals("true")){
						if(user.canActivate(expt.getItem()))expt.quarantine(user);
						else this.getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN,"Specified user account has insufficient activation priviledges for experiments in this project.");
					}

					if(user.canEdit(expt.getItem())){
						if((this.isQueryVariableTrue(XNATRestConstants.PULL_DATA_FROM_HEADERS) || this.containsAction(XNATRestConstants.PULL_DATA_FROM_HEADERS) ) && expt instanceof XnatImagesessiondata){
							try {
								PullSessionDataFromHeaders pull=new PullSessionDataFromHeaders((XnatImagesessiondata)expt,user,this.allowDataDeletion(),this.isQueryVariableTrue("overwrite"),false);
								pull.call();
							} catch (SAXException e){
								logger.error("",e);
								this.getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,e.getMessage());
							} catch (ValidationException e){
								logger.error("",e);
								this.getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,e.getMessage());
							} catch (Exception e) {
								logger.error("",e);
								this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL,e.getMessage());
								return;
							}
						}
						
						if(this.isQueryVariableTrue(XNATRestConstants.TRIGGER_PIPELINES) || this.containsAction(XNATRestConstants.TRIGGER_PIPELINES)){
							TriggerPipelines tp=new TriggerPipelines(expt,true,this.isQueryVariableTrue(XNATRestConstants.SUPRESS_EMAIL),user);
							tp.call();
						}
					}
				}

				this.returnString(expt.getId(),(existing==null)?Status.SUCCESS_CREATED:Status.SUCCESS_OK);
				}else{
					this.getResponse().setStatus(Status.CLIENT_ERROR_UNPROCESSABLE_ENTITY,"Only xnat:Subject documents can be PUT to this address.");
				}
		} catch (InvalidValueException e) {
			this.getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
			logger.error("",e);
			} catch (Exception e) {
				this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL);
			logger.error("",e);
		}
	}
	

	@Override
	public boolean allowDelete() {
		return true;
	}

	@Override
	public void handleDelete(){

			if(expt==null&& exptID!=null){
				expt=(XnatSubjectassessordata)XnatExperimentdata.getXnatExperimentdatasById(exptID, user, false);
				
				if(expt==null && this.proj!=null){
				expt=(XnatSubjectassessordata)XnatExperimentdata.GetExptByProjectIdentifier(this.proj.getId(), exptID,user, false);
				}
			}
			
			if(expt==null){
			this.getResponse().setStatus(Status.CLIENT_ERROR_NOT_FOUND,"Unable to find the specified experiment.");
				return;
			}
			
		XnatProjectdata newProject=null;
			
		if(filepath!=null && !filepath.equals("")){
			if(filepath.startsWith("projects/")){
				String newProjectS= filepath.substring(9);
				newProject=XnatProjectdata.getXnatProjectdatasById(newProjectS, user, false);
				if(newProject==null){
					this.getResponse().setStatus(Status.CLIENT_ERROR_NOT_FOUND,"Unable to identify project: " + newProjectS);
					return;
				}
			}else{
					this.getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST);
					return;
	                }
		}else if(!expt.getProject().equals(proj.getId())){
			newProject=proj;
	            }
	            
		String msg=expt.delete((newProject!=null)?newProject:proj, user, this.isQueryVariableTrue("removeFiles"));
		if(msg!=null){
			this.getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN,msg);
			return;
		}
	}

	@Override
	public Representation getRepresentation(Variant variant) {	
		MediaType mt = overrideVariant(variant);

		if(expt==null&& exptID!=null){
			expt=(XnatSubjectassessordata)XnatExperimentdata.getXnatExperimentdatasById(exptID, user, false);
			
			if(expt==null && this.proj!=null){
				expt=(XnatSubjectassessordata)XnatExperimentdata.GetExptByProjectIdentifier(this.proj.getId(), exptID,user, false);
			}
		}
		
		if(expt!=null){
			if(filepath!=null && !filepath.equals("") && filepath.equals("status")){
				try {
					if(expt.needsActivation()){
					    return new StringRepresentation("quarantine",mt);
					}else{
					    return new StringRepresentation("active",mt);
					}
				} catch (Exception e) {
					logger.error("",e);
				    return new StringRepresentation("active",mt);
			}
			}else if(filepath!=null && !filepath.equals("") && filepath.startsWith("projects")){
				XFTTable t = new XFTTable();
				ArrayList al = new ArrayList();
				al.add("label");
				al.add("ID");
				al.add("Secondary_ID");
				al.add("Name");
				t.initTable(al);

				Object[] row=new Object[4];
				row[0]=expt.getLabel();
				XnatProjectdata primary = expt.getPrimaryProject(false);
				row[1]=primary.getId();
				row[2]=primary.getSecondaryId();
				row[3]=primary.getName();
				t.rows().add(row);

				for(Map.Entry<XnatProjectdataI, String> entry: expt.getProjectDatas().entrySet()){
					row=new Object[4];
					row[0]=entry.getValue();
					row[1]=entry.getKey().getId();
					row[2]=entry.getKey().getSecondaryId();
					row[3]=entry.getKey().getName();
					t.rows().add(row);
				}

				return representTable(t, mt, new Hashtable<String,Object>());
		}else{
				return this.representItem(expt.getItem(),mt);
			}
		} else {
			this.getResponse().setStatus(Status.CLIENT_ERROR_NOT_FOUND,
					"Unable to find the specified experiment.");
			return null;
		}

	}

}
